import { Component } from '@angular/core';
import { FeaturesPaises } from '../../interfaces/features-paises.interface';
import { ApiPaisesService } from '../../services/api-paises.service';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styleUrls: ['./por-region.component.css']
})
export class PorRegionComponent {

  regiones: string[] = ['africa', 'americas', 'asia', 'europe', 'oceania'];
  regionActiva: string= '';
  paises: FeaturesPaises[] = [];

  constructor(private apiPaisesService: ApiPaisesService) { }

  activarRegion(regionSelected: string) {
    console.log(regionSelected);
    this.regionActiva = regionSelected;

    this.apiPaisesService.buscarRegion( regionSelected )
      .subscribe( paises => {
        console.log(paises);
        this.paises = paises;
      })

    // TODO: hacer el llamado al servicio
  }

}
