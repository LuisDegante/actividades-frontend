import { Component } from '@angular/core';
import { FeaturesPaises } from '../../interfaces/features-paises.interface';
import { ApiPaisesService } from '../../services/api-paises.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html',
  styleUrls: ['./por-capital.component.css']
})
export class PorCapitalComponent {

  termino: string = '';
  existeError:boolean = false;
  paises: FeaturesPaises[] = [];
  

  constructor(private apiPaisesService: ApiPaisesService) { }

  buscar(termino: string) {
    this.existeError = false;
    this.termino = termino;

    this.apiPaisesService.buscarCapital(termino)
      .subscribe((capitales) => {
        console.log(capitales);
        this.paises = capitales;
        
      }, (error) => {
        this.existeError = true;
        this.paises = [];
      });
  }
}
