import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { FeaturesPaises } from '../interfaces/features-paises.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiPaisesService {

  private apiPaises: string = 'https://restcountries.com/v2/';
  // private apiPaises2: string = 'https://restcountries.com/v3.1/'

  constructor(private http: HttpClient) { }

  buscarPais(item: string): Observable<FeaturesPaises[]> {

    const url = `${this.apiPaises}name/${item}`;
    return this.http.get<FeaturesPaises[]>(url);
  }

  buscarCapital(item: string): Observable<FeaturesPaises[]> {

    const url = `${this.apiPaises}capital/${item}`;
    return this.http.get<FeaturesPaises[]>(url);
  }

  buscarRegion(item: string): Observable<FeaturesPaises[]> {

    const url = `${this.apiPaises}region/${item}`;
    return this.http.get<FeaturesPaises[]>(url);
  }

}
