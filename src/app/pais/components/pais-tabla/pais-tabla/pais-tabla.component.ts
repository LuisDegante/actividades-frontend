import { Component, Input } from '@angular/core';

import { FeaturesPaises } from '../../../interfaces/features-paises.interface';
import { ApiPaisesService } from '../../../services/api-paises.service';

@Component({
  selector: 'app-pais-tabla',
  templateUrl: './pais-tabla.component.html',
  styleUrls: ['./pais-tabla.component.css']
})
export class PaisTablaComponent {

  @Input() paises: FeaturesPaises[] = [];

  constructor(private apiPaisesService: ApiPaisesService) { }


}
