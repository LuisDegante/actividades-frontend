import { Component } from '@angular/core';

import { ApiPaisesService } from '../../services/api-paises.service';
import { FeaturesPaises } from '../../interfaces/features-paises.interface';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styleUrls: ['./por-pais.component.css']
})
export class PorPaisComponent {

  termino: string = '';
  existeError: boolean = false;
  paises: FeaturesPaises[] = [];

  constructor(private apiPaisesService: ApiPaisesService) { }

  buscar(termino: string) {
    this.existeError = false;
    this.termino = termino;

    this.apiPaisesService.buscarPais(termino)
      .subscribe((paises) => {
        console.log(paises);
        this.paises = paises;
        
      }, (error) => {
        this.existeError = true;
        this.paises = [];
      });
  }
}
